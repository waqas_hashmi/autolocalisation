# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Introduction:
So basically this plugin can be used for language Translation in unity. There are other translator on asset store but they charge high cost for providing auto translation.

You can use this plugin for translating your words in game such as some time English is your 2nd language and it is difficult for you to understand it and you change the language by going in settings of game. You can do it using this plugin.

Purpose:

•	You can change from 2nd language to your native language by using it in your game.

•	Thousands of people around the world can understand your game

Features: 

1.	You don’t need to put the meaning of each word manually. After press Translate Button, translation downloaded from Google.

2.	We are not using Google API. So this package will not effect the size of your build.

3.	Code is Clean you can change it manually.

4.	This plugin support multiple international languages.

5.	You can use its “Translate Script” in any project.

6.	We are working on it for making it more robust.


### How do I get set up? ###

Full Documentation PDF is in AutoLocalisatoin Folder.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###
https://programminglearner143.blogspot.com/2017/09/auto-localization.html

Email: waqashaxhmi143@gmail.com