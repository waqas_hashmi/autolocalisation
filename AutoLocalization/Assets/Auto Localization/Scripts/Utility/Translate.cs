﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;// use for parsing json text
using UnityEditor;

namespace LanguageTranslator{
	public class Translate : MonoBehaviour{
		private string translatedText = "";

		// DB path + DB object for reference 
		private const string DATABASE_PATH = @"Assets/Auto Localization/Resources/LanguageDB/LanguageDatabase.asset";
		private LanguageDatabase db;

		[SerializeField]
		ProgressBarController _controller;
		void Start(){
			db = (LanguageDatabase)AssetDatabase.LoadAssetAtPath (DATABASE_PATH, typeof(LanguageDatabase));
			db.Translate (() => {
				_controller.ProgressComplete();
			});
		}

		public void Translation(LanguageCodes source,List<LanguageCodes> target,string word){
			StartCoroutine (Process (source, target, word));
		}
		private int translatedWord;
		private IEnumerator Process (LanguageCodes sourceLang, List<LanguageCodes> targetLang, string sourceText) {

			string sourcText = sourceLang.ToString ();
			bool isError = false;
			foreach (var val in targetLang) {
				string targetText = val.ToString ();
				string url = "https://translate.googleapis.com/translate_a/single?client=gtx&sl="
				             + sourcText + "&tl=" + targetText + "&dt=t&q=" + WWW.EscapeURL (sourceText);

				WWW www = new WWW (url);
				yield return www;

				if (www.isDone) {
					if (string.IsNullOrEmpty (www.error)) {
						var N = JSONNode.Parse (www.text);// Json Parser
						translatedText = N [0] [0] [0];
						print (translatedText);
						OnTranslatedWord (val, translatedText, sourceText);
					} else {
						Debug.LogError ("Internet not working properly");
						isError = true;
						yield break;
					}
				} 

			}
			if (!isError) {
				_controller.ValueChange (db.GetDB ().Count, db.GetList ().Count);
				db.OnTranslated ();
			}

		}
		private void OnTranslatedWord(LanguageCodes target,string targetText,string sourceText){
			WordTranslation wordT = new WordTranslation ();
			wordT.country = (Languages)target;
			wordT.meaning = targetText;
			db.GetDB ().Find (x => x.word.Equals (sourceText)).wordTranslation.Add (wordT);
		}
	}
}