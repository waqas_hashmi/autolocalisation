﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum Languages{
	Arabic =0,
	English =1,
	Filipino=2,
	French =3,
	German=4,
	Hindi=5,
	Indonesian=6,
	Japanese=7,
	Russian=8,
	Spanish =9,
	Turkish=10,
	Urdu = 11
}
namespace LanguageTranslator{
	public enum LanguageCodes{
		ar=0,
		en=1,
		tl=2,
		fr=3,
		de=4,
		hi=5,
		id=6,
		ja=7,
		ru=8,
		es=9,
		tr=10,
		ur=11

	}
	public class LanguageDatabase : ScriptableObject {

		[SerializeField]
		private List<Word> database;
		[SerializeField]
		private List<LanguageData> data;
		[SerializeField]
		private int successfullDownloaded =0;
		public Languages sourceLanguage;

		void OnEnable(){
			
			if (data == null || database == null) {
				data = new List<LanguageData> ();
				database = new List<Word>();
				Debug.Log ("data is Null");
			}
		}
		public void Add(LanguageData d){
			data.RemoveAll (x => x.word.Equals (d.word));
			this.data.Add (d);
		}

		public void Remove(int index){
			Debug.Log (index);
			if (index < data.Count) {
				data.RemoveAt (index);
				if (data.Count == 1) {
					database.Clear ();
					successfullDownloaded = 0;
				}
			}
			if (index < database.Count) {
				database.RemoveAt (index);
				successfullDownloaded--;
			}

		}
		public List<LanguageData> GetList(){
			return data;
		}
		public List<Word> GetDB(){
			return database;
		}
		public void Translate(System.Action callback){
			if (successfullDownloaded < data.Count) {
				var value = data [successfullDownloaded];
				int i = database.RemoveAll (x => x.word.Equals (value.word));
				Word word = new Word ();
				word.word = value.word;
				word.wordTranslation = new List<WordTranslation> ();
				database.Add (word);
				GetTargetLanguage (value.translatedTo, value.word);
			} else {
				if(callback != null)
					callback ();
			}
		}
		public void OnTranslated(){
			successfullDownloaded++;
			UnityEditor.EditorUtility.SetDirty (this);
			Debug.Log ("---------------------------------------------------------");
			Translate (null);
		}
		private void GetTargetLanguage(int selectedEnum,string word){

			int value =(int) selectedEnum;
			List<LanguageCodes> list = new List<LanguageCodes> ();
			int count = 0;
			int max = System.Enum.GetNames(typeof(LanguageCodes)).Length;
			while (count < max) {
				int isOne = (value & 1);
				value = value >> 1;
				if (isOne == 1){
					list.Add((LanguageCodes)count);
				}
				count++;
			}
			LanguageCodes sourcecode =(LanguageCodes) sourceLanguage;
			Translate translate = GameObject.FindObjectOfType<Translate>();
			translate.Translation (sourcecode, list, word);

		}



	}
	[System.Serializable]
	public class LanguageData
	{
		public int translatedTo;
		public string word;
		public LanguageData(){
			word="";
		}
	}
	[System.Serializable]
	public class Word{
		public string word ;
		public List<WordTranslation> wordTranslation;
	}
	[System.Serializable]
	public class WordTranslation{// translation of word in different Languages
		public Languages country;
		public string meaning;

	}
}
